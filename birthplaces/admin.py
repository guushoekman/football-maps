from django.contrib import admin

from . import models

admin.site.register(models.BirthPlaces_Game)
admin.site.register(models.BirthPlaces_Team)
admin.site.register(models.BirthPlaces_Player)
admin.site.register(models.BirthPlaces_Country)
admin.site.register(models.BirthPlaces_City)