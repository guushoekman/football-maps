import csv, io
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Count
from .models import *

def birthplaces_index(request):
	games = BirthPlaces_Game.objects.order_by("date", "home_team")
	players = BirthPlaces_Player.objects.order_by("name")
	context = {
		"games": games,
		"players": players,
	}

	return render(request, "index.html", context)

def birthplaces_allplayers(request):
	players = BirthPlaces_Player.objects.order_by("name")
	cities = BirthPlaces_City.objects.all().annotate(player_count=Count("birthplaces_player")).order_by("-player_count", "country", "name")
	countries = BirthPlaces_Country.objects.all().annotate(player_count=Count("birthplaces_city"))
	context = {
		"players": players,
		"cities": cities,
		"countries": countries.order_by("player_count").reverse(),
	}

	return render(request, "all_players.html", context)

def game_ajax(request, id):
    game = BirthPlaces_Game.objects.get(pk=id)
    context = {
    	"game": game,
    }

    return render(request, "game.ajax.html", context)

def country_upload(request):
	template = "country_upload.html"

	prompt = {
		'order': 'Order of csv name, slug, capital'
	}

	if request.method == "GET":
		return render(request, template, prompt)

	csv_file = request.FILES['country']

	data_set = csv_file.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	next(io_string)
	for column in csv.reader(io_string, delimiter=',', quotechar="|"):
		_, created = BirthPlaces_Country.objects.update_or_create(
			name=column[0],
			slug=column[1],
			capital=column[2]
		)
	context = {}
	return render(request, template, context)

def city_upload(request):
	template = "city_upload.html"

	prompt = {
		'order': 'Order of csv name, country, lat, lon'
	}

	if request.method == "GET":
		return render(request, template, prompt)

	csv_file = request.FILES['city']

	data_set = csv_file.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	next(io_string)
	for column in csv.reader(io_string, delimiter=',', quotechar="|"):
		_, created = BirthPlaces_City.objects.update_or_create(
			name=column[0],
			country=BirthPlaces_Country.objects.get(name=str(column[1])),
			lat=column[2],
			lon=column[3]
		)
	context = {}
	return render(request, template, context)

def player_upload(request):
	template = "player_upload.html"

	prompt = {
		'order': 'Order of csv name, dob, pob, team'
	}

	if request.method == "GET":
		return render(request, template, prompt)

	csv_file = request.FILES['player']

	data_set = csv_file.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	next(io_string)
	for column in csv.reader(io_string, delimiter=',', quotechar="|"):
		_, created = BirthPlaces_Player.objects.update_or_create(
			name=column[0],
			dob=column[1],
			pob=BirthPlaces_City.objects.get(name=str(column[2])),
			team=BirthPlaces_Team.objects.get(name=str(column[3])),
		)
	context = {}
	return render(request, template, context)

def team_upload(request):
	template = "team_upload.html"

	prompt = {
		'order': 'Order of csv name, city, stadium, lat, lon'
	}

	if request.method == "GET":
		return render(request, template, prompt)

	csv_file = request.FILES['team']

	data_set = csv_file.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	next(io_string)
	for column in csv.reader(io_string, delimiter=',', quotechar="|"):
		_, created = BirthPlaces_Team.objects.update_or_create(
			name=column[0],
			city=BirthPlaces_City.objects.get(name=str(column[1])),
			stadium=column[2],
			lat=column[3],
			lon=column[4],
		)
	context = {}
	return render(request, template, context)

def game_upload(request):
	template = "game_upload.html"

	prompt = {
		'order': 'Order of csv date, home_team, away_team, home_score, away_score'
	}

	if request.method == "GET":
		return render(request, template, prompt)

	csv_file = request.FILES['game']

	data_set = csv_file.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	next(io_string)
	for column in csv.reader(io_string, delimiter=',', quotechar="|"):
		_, created = BirthPlaces_Game.objects.update_or_create(
			date=column[0],
			home_team=BirthPlaces_Team.objects.get(name=str(column[1])),
			away_team=BirthPlaces_Team.objects.get(name=str(column[2])),
			home_score=column[3],
			away_score=column[4],
		)
	context = {}
	return render(request, template, context)