from django.apps import AppConfig


class BirthplacesConfig(AppConfig):
    name = 'birthplaces'
