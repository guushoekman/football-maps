from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()

class BirthPlaces_Country(models.Model):
	name = models.CharField(max_length=50, null=True)
	slug = models.CharField(max_length=10, null=True)
	capital = models.CharField(max_length=50, null=True)

	class Meta:
		verbose_name_plural = "Countries"
		ordering = ["name"]

	def __str__(self):
		return self.name

class BirthPlaces_City(models.Model):
	name = models.CharField(max_length=50, null=True)
	country = models.ForeignKey(BirthPlaces_Country, on_delete=models.CASCADE, null=True)
	lat = models.DecimalField(max_digits=9, decimal_places=6, null=True)
	lon = models.DecimalField(max_digits=9, decimal_places=6, null=True)

	class Meta:
		ordering = ["name"]
		verbose_name_plural = "Cities"

	def __str__(self):
		return self.name

class BirthPlaces_Team(models.Model):
	name = models.CharField(max_length=50, null=True)
	city = models.ForeignKey(BirthPlaces_City, on_delete=models.CASCADE, null=True)
	stadium = models.CharField(max_length=50, null=True)
	lat = models.DecimalField(max_digits=9, decimal_places=6, null=True)
	lon = models.DecimalField(max_digits=9, decimal_places=6, null=True)

	class Meta:
		ordering = ["name"]
		verbose_name_plural = "Teams"

	def __str__(self):
		return self.name

class BirthPlaces_Player(models.Model):
	name = models.CharField(max_length=50, null=True)
	dob = models.DateField(null=True)
	pob = models.ForeignKey(BirthPlaces_City, on_delete=models.CASCADE, null=True)
	team = models.ForeignKey(BirthPlaces_Team, on_delete=models.CASCADE, null=True)

	class Meta:
		ordering = ["team", "name"]
		verbose_name_plural = "Players"

	def __str__(self):
		return self.team.name + " - " + self.name

class BirthPlaces_Game(models.Model):
	date = models.DateField(null=True)
	home_team = models.ForeignKey(BirthPlaces_Team, related_name='home_team', on_delete=models.CASCADE, null=True)
	away_team = models.ForeignKey(BirthPlaces_Team, related_name='away_team', on_delete=models.CASCADE, null=True)
	home_score = models.IntegerField(null=True)
	away_score = models.IntegerField(null=True)
	players = models.ManyToManyField(BirthPlaces_Player)

	class Meta:
		ordering = ["date", "home_team"]
		verbose_name_plural = "Games"

	def __str__(self):
		return self.home_team.name + " - " + self.away_team.name