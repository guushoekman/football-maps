from django.urls import path

from . import views

urlpatterns = [
    path('', views.birthplaces_index, name='birthplaces_index'),
    path('allplayers', views.birthplaces_allplayers, name='birthplaces_allplayers'),
    path("game/<int:id>/", views.game_ajax, name="game_ajax"),
    path('country-upload', views.country_upload, name='country_upload'),
    path('city-upload', views.city_upload, name='city_upload'),
    path('player-upload', views.player_upload, name='player_upload'),
    path('team-upload', views.team_upload, name='team_upload'),
    path('game-upload', views.game_upload, name='game_upload'),
]