from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('birthplaces/', include('birthplaces.urls')),
    path('admin/', admin.site.urls),
]